Role Name
=========

Installs an vaultwarden docker image

Requirements
------------

Python3 and docker need to be installed

Role Variables
--------------

Look in the defaults directory
vaultwarden_docker_labels: {}

Dependencies
------------

depends on vaultwarden docker image

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

GNU Affero General Public License v3

Author Information
------------------

Famedly GmbH, famedly.de
